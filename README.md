### usage

Here's an example of basic usage:

```js
<SVCalendar
	mode="range"
	startDate={new Date(2021, 1, 1)}
	minDate={new Date(2021, 1, 13)}
	availableYears={[2023, 2020]}
	on:startDate={(event) => console.log('startDate', event.detail)}
	on:endDate={(event) => console.log('endDate', event.detail)}
/>
```

### props

| Prop name      | Description                                                                                                                                                                             | Default value                                                           | Example values           |
| -------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------- | ------------------------ |
| mode           | `single` allow selection of a single date, only `startDate` will be emitted for this case. `range` allows selectin of two dates `startDate` and `endDate` will be emitted for this case | single                                                                  | ` 'single'` or`'range' ` |
| startDate      | The beginning of a period that shall be displayed.                                                                                                                                      | new Date()                                                              | new Date(2017, 0, 1)     |
| maxDate        | Maximum date that the user can select.                                                                                                                                                  | null                                                                    | new Date()               |
| minDate        | Minimum date that the user can select.                                                                                                                                                  | null                                                                    | new Date()               |
| availableYears | range of years to be displayed on the calender year dropdown. startDate year should be in this range                                                                                    | [Number(new Date().getFullYear) - 5,Number(new Date().getFullYear) + 5] | [2016, 2025]             |
| startDate      | event emitted when start date is selected                                                                                                                                               | n/a                                                                     |                          |
| endDate        | event emitted when start date is selected                                                                                                                                               | n/a                                                                     |                          |
