import { addMonths, format, subMonths } from 'date-fns';
export const months: string[] = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December'
];

const monthDays: number[] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

const isLeapYear = (year: number): boolean => year % 4 === 0;

export const weekDays: string[] = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];

export const getMonthName = (index: number): string => {
	if (!index) return;
	return months[index];
};

export const getMonthIndex = (mnth: string): number => {
	if (!mnth) return;
	return months.findIndex((month) => month === mnth);
};

export const getMonthDays = (index: number, year: number): number => {
	if (!index && !year) return;
	return index !== 1 ? monthDays[index] : isLeapYear(year) ? 29 : monthDays[index];
};

export const getDateRows = (monthIndex: number, year: number): Date[] => {
	const currentDate: Date = new Date(year, monthIndex, 1);

	const monthBefore: Date = subMonths(currentDate, 1);
	const monthAfter: Date = addMonths(currentDate, 1);

	const monthDays: Date[] = [];
	const daysBefore: Date[] = [];
	const daysAfter: Date[] = [];

	const days = Array(getMonthDays(monthIndex, year) ?? 0).fill(0);

	const daysOfMonthBefore = Array(
		getMonthDays(monthIndex == 0 ? monthIndex : monthIndex - 1, year) ?? 0
	)
		.fill(0)
		.map((_, index) => index + 1);
	days.forEach((_, index) =>
		monthDays.push(new Date(currentDate.getFullYear(), currentDate.getMonth(), index + 1))
	);

	if (currentDate.getDay()) {
		daysOfMonthBefore
			.slice(-currentDate.getDay())
			.forEach((el) =>
				daysBefore.push(new Date(monthBefore.getFullYear(), monthBefore.getMonth(), el))
			);
	}

	const appendedDaysCount = 7 - ([...daysBefore, ...monthDays].length % 7);
	Array(appendedDaysCount)
		.fill(0)
		.forEach((_, index) =>
			daysAfter.push(new Date(monthAfter.getFullYear(), monthAfter.getMonth(), index + 1))
		);

	return [...daysBefore, ...monthDays, ...daysAfter];
};
